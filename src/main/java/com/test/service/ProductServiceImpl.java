package com.test.service;

import com.test.dao.ProductDao;
import com.test.dao.ProductJpaDao;
import com.test.domain.Product;

/**
 * Created by Andrew on 26.10.2015.
 */
public class ProductServiceImpl implements ProductService {

    public ProductDao productDao;

    public ProductServiceImpl() {
        productDao = new ProductJpaDao();
    }


    public void add(Product product) {
        productDao.add(product);
    }

    public void sell(Product product) {
        productDao.sell(product);
    }
}
