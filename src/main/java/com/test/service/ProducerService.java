package com.test.service;

import com.test.domain.Producer;

/**
 * Created by Andrew on 26.10.2015.
 */
public interface ProducerService {
    void addProducer (Producer producer);
}
