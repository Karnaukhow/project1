package com.test.service;

import com.oracle.webservices.internal.api.databinding.Databinding;
import com.test.domain.Producer;
import com.test.domain.Product;

/**
 * Created by Andrew on 26.10.2015.
 */
public class ProductBuilder {

    private long id;
    private String name;
    private double price;
    private int amount;
    private double margin;
    private Producer producer;

    public ProductBuilder id (long id) { this.id = id;  return this; }
    public ProductBuilder name (String name) { this.name = name;  return this; }
    public ProductBuilder price (double price) { this.price = price;  return this; }
    public ProductBuilder amount (int amount) { this.amount = amount;  return this; }
    public ProductBuilder margin (double margin) { this.margin = margin;  return this; }
    public ProductBuilder producer (Producer producer) { this.producer = producer;  return this; }
    public Product build() {return Builder(this);}

    private Product Builder(ProductBuilder builder) {
        Product product = new Product();
        product.setId(builder.id);
        product.setName(builder.name);
        product.setPrice(builder.price);
        product.setAmount(builder.amount);
        product.setMargin(builder.margin);
        product.setProducer(builder.producer);

        return product;
    }
}
