package com.test.service;

import com.test.domain.Product;

/**
 * Created by Andrew on 26.10.2015.
 */
public interface ProductService {

    public void add(Product product);
    public void sell(Product product);
}
