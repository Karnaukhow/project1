package com.test.service;

import com.test.dao.ProducerDao;
import com.test.dao.ProducerJpaDao;
import com.test.domain.Producer;

/**
 * Created by Andrew on 26.10.2015.
 */
public class ProducerServiceImpl implements ProducerService{

    private ProducerDao producerDao;

    public ProducerServiceImpl() {
        this.producerDao = new ProducerJpaDao();
    }

    public void addProducer(Producer producer) {
        this.producerDao.add(producer);
    }
}
