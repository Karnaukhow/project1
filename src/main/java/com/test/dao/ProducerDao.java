package com.test.dao;

import com.test.domain.Producer;

/**
 * Created by Andrew on 26.10.2015.
 */
public interface ProducerDao {
    void add (Producer producer);
    
}
