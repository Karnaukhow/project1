package com.test.dao;

import com.test.domain.Producer;
import com.test.util.HibernateUtil;
import org.hibernate.Session;

/**
 * Created by Andrew on 26.10.2015.
 */
public class ProducerJpaDao implements  ProducerDao{
    public void add(Producer producer) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(producer);
        session.getTransaction().commit();
        session.close();
    }
}
