package com.test.dao;

import com.test.domain.Product;
import com.test.domain.Statistic;
import com.test.util.HibernateUtil;
import org.hibernate.Session;

import java.util.Calendar;

/**
 * Created by Andrew on 26.10.2015.
 */
public class ProductJpaDao implements ProductDao {

    public void add(Product product) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(product);
        session.getTransaction().commit();
        session.close();

    }

    public void sell(Product product) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        product.setAmount(product.getAmount()-1);
        session.update(product);

        Statistic stat = new Statistic();
        stat.setProduct(product);
        stat.setAmount(1);
        stat.setSale_date(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));

        session.save(product);
        session.save(stat);
        session.getTransaction().commit();
        session.close();

    }
}
