package com.test.dao;

/**
 * Created by Andrew on 26.10.2015.
 */
import com.test.domain.Product;
public interface ProductDao {
    void add(Product product);
    void sell(Product product);
}
