package com.test.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Andrew on 26.10.2015.
 */
@Entity(name = "statistic")
public class Statistic {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int Id;

    @ManyToOne(targetEntity = Product.class)
    private Product product;

    @Column
    private int amount;

    @Column
    private Date sale_date;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getSale_date() {
        return sale_date;
    }

    public void setSale_date(Date sale_date) {
        this.sale_date = sale_date;
    }
}
