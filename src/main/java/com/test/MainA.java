package com.test;

import com.test.domain.Producer;
import com.test.service.ProducerService;
import com.test.service.ProducerServiceImpl;

/**
 * Created by Andrew on 26.10.2015.
 */
public class MainA {
    public static void main(String[] args) {
        ProducerService producerService = new ProducerServiceImpl();
        Producer producer = new Producer();
        producer.setName("Snickers");
        producerService.addProducer(producer);
    }
}
